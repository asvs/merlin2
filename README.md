[Architecture, Design and Threat Modeling Verification Requirements](https://gitlab.com/asvs/merlin2/-/milestones/6)

[Authentication Verification Requirements](https://gitlab.com/asvs/merlin2/-/milestones/11)

[Session Management Verification Requirements](https://gitlab.com/asvs/merlin2/-/milestones/12)

[Access Control Verification Requirements](https://gitlab.com/asvs/merlin2/-/milestones/7)

[Validation, Sanitization and Encoding Verification Requirements](https://gitlab.com/asvs/merlin2/-/milestones/13)

[Stored Cryptography Verification Requirements](https://gitlab.com/asvs/merlin2/-/milestones/2)

[Error Handling and Logging Verification Requirements](https://gitlab.com/asvs/merlin2/-/milestones/4)

[Data Protection Verification Requirements](https://gitlab.com/asvs/merlin2/-/milestones/8)

[Communications Verification Requirements](https://gitlab.com/asvs/merlin2/-/milestones/9)

[Malicious Code Verification Requirements](https://gitlab.com/asvs/merlin2/-/milestones/14)

[Business Logic Verification Requirements](https://gitlab.com/asvs/merlin2/-/milestones/1)

[Files and Resources Verification Requirements](https://gitlab.com/asvs/merlin2/-/milestones/5)

[API and Web Service Verification Requirements](https://gitlab.com/asvs/merlin2/-/milestones/10)

[Configuration Verification Requirements](https://gitlab.com/asvs/merlin2/-/milestones/3)

